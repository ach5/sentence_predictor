import sys
from config import settings
from typing import List
# import wandb 
from sklearn.metrics import average_precision_score, f1_score
import pandas as pd, numpy as np
import tensorflow as tf
import tensorflow.keras.backend as K
from sklearn.model_selection import StratifiedKFold
from transformers import *
import tokenizers
import os
import re
from typing import Union
import json 
import pandas as pd
import math
import rules_spacy_model
from rules_spacy_model import nlp
print('TF version',tf.__version__)


def json_parser(ann_path: str, interp_path: str):
    def cat_map_new_cruncher(s: Union[dict, str]) -> str:
        """ Convert new curncher categories into annotation categories.
        :param s: unmapped interpretation category
        :return str: mapped category
        """
        REG_SNAKE = re.compile(r'(?<!/)(?=[A-Z])')
        REG_NOVALUE = re.compile(r'(/value|/percents|/name|/method|/permitted)$')
        # pat = r'\[\d\]|\/Subrules|\/Method|\/Ignore$|\/Permitted$|Sentence$'
        pat = r'\[\d\]'
        if type(s) == str:
            # old protocol
            no_digits = re.sub(pat, "[0]", s)
        elif type(s) == dict:
            # new protocol
            no_digits = re.sub(pat, "[0]", s['category'])

        snake_case = REG_SNAKE.sub('_', no_digits).lower()
        # TODO: remove after data migration
        snake_case = snake_case.replace('definition_sentence', 'definition')
        snake_case = REG_NOVALUE.sub('', snake_case)

        r_trailing_index = r'\/?\[\d+\]$'
        remove_trailing_index = re.sub(r_trailing_index, "", snake_case)

        r_residual_amounts = r'residual_amounts?\/?(ignore|deduct_from)?$'
        fix_residual_amounts = re.sub(r_residual_amounts, "residual_amount", remove_trailing_index)

        final = fix_residual_amounts
        return final

    f = open(ann_path)
    ann = json.load(f)

    # Opening JSON file
    f = open(interp_path)
    rules = json.load(f)

    ann_k = [x['id'] for x in ann]
    comm_keys = set(rules.keys()) & set(ann_k)

    ann = [x for x in ann if x['id'] in comm_keys]
    DROP_CLASSES = [
        'change/permission',
        'cancellation/fee/calculation',
        'cancellation/fee_per'
    ]
    sents = []
    for r in ann:
        for sent in r['sentences']:
            a, z = sent['index']
            text = rules[r['id']]['raw_text'][a:z]
            is_valid = rules[r['id']]['validation_finished']
            y = [cat_map_new_cruncher(v['category']) for v in sent['category']['user_entry']]
            y = [x.replace('/subrules[0]/', '') for x in y if not x.startswith('/markup')]

            y = [x for x in set(y) if x not in DROP_CLASSES]

            permission = True
            try:
                permission: bool = rules[r['id']]['subrules'][0]['cancellation']['permission'][0]['permitted']

            except:
                permission = True

            if permission:
                # убираем cancellation/permission True
                y = [x for x in y if x != 'cancellation/permission' and permission]

            if not y:
                # если y пустой
                y = ['-']
            row = []
            row.append(r['id'])
            row.append(text)
            row.append(y)
            row.append(is_valid)
            sents.append(row)

    df = pd.DataFrame(sents, columns=['rid', 'sent', 'y', 'valid'])

    df = df[df['valid']].iloc[:, :-1].reset_index(drop=True)

    return df


def clean_text(text):
    text = ' '.join(text.split())
    if text:
        text = nlp(text)._.normalize()
        for entity in '[[Percent]] [[Money]] [[Period]] [[Date]] [[Tax]]'.split():
            if entity in text:
                text = text.replace(entity, entity[2:-2])
        return text
    return text

def get_target(df):
    target = []
    for _ in df.y:
        targ_current = [settings.cat2num[t] for t in _ if t in settings.cat2num]
        target.append([int(x in targ_current) for x in range(len(settings.cat2num))])
    return np.array(target)

def get_tokens(sents: List[str]):
    train = []
    for i in range(1, len(sents)+1):
        a, b = max(i-settings.win_size, 0), i
        train.append(''.join(sents[a:b]))


    ct = len(train)
    input_ids = np.ones((ct, settings.MAX_LEN),dtype='int32')
    attention_mask = np.zeros((ct, settings.MAX_LEN),dtype='int32')
    token_type_ids = np.zeros((ct, settings.MAX_LEN),dtype='int32')

    for k in range(ct):
        text = train[k]
        text = ' '.join(text.split())
        enc = settings.tokenizer.encode(text) 

        input_ids[k,:len(enc.ids)+2] = [0] + enc.ids + [2]
        attention_mask[k,:len(enc.ids)+2] = 1
    return [input_ids, attention_mask, token_type_ids]
    
    
def get_tokens_target(df):
    target = get_target(df)

    input_ids = []
    attention_mask = []
    token_type_ids = []
    for i in df['rid'].unique():
        sents = df[df['rid'] == i].sent.tolist()
        input_ids_, attention_mask_, token_type_ids_ = get_tokens(sents)
        input_ids.append(input_ids_)
        attention_mask.append(attention_mask_)
        token_type_ids.append(token_type_ids_)
    input_ids = np.concatenate(input_ids)
    attention_mask = np.concatenate(attention_mask)
    token_type_ids = np.concatenate(token_type_ids)
    return [input_ids, attention_mask, token_type_ids], target


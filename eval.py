import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
os.environ['TOKENIZERS_PARALLELISM'] = 'true'
 
from model import build_model, load_weights, get_metrics
from preprocessing import json_parser, get_tokens_target
import pandas as pd

def eval(run_name, l_model_name, n_folds)
    print('Start eval')
    all_metrics = []
    for i in range(n_folds):
        model = build_model()

        model = load_weights(model, f'{run_name}-{l_model_name}-{i}.h5')

        df = json_parser('data/annotations.json', 'data/interpretations.json')

        test_ids = pd.read_csv('test_ids.csv').iloc[:,0]

        df = df[df.rid.isin(test_ids)].reset_index(drop=True)

        tokens, target  = get_tokens_target(df)

        pred = model.predict(tokens)

        metrics = get_metrics(target, pred, df)

        all_metrics.append(metrics)
    return all_metrics

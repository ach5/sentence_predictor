import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

from preprocessing import json_parser, get_tokens_target
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.model_selection import StratifiedKFold
from config import settings
from model import build_model, get_metrics, get_skf_target, save_weights
import numpy as np
import math
from sklearn.metrics import average_precision_score, f1_score

import pandas as pd
import wandb
import tensorflow.keras.backend as K

wandb.init(project="cruncher-sentence-categories", entity='cteleport1')


wandb.config.max_len = MAX_LEN = 220
wandb.config.epochs = EPOCHS = 5 # originally 3
wandb.config.batch_size = BATCH_SIZE = 32 # originally 32
wandb.config.lr = LR = 3e-5
wandb.config.lang_model = 'Roberta'
wandb.config.win_sents_size = 3
wandb.config.tokenizer = 'new_tokenizer'

df = json_parser('data/annotations.json', 'data/interpretations.json')

valids = pd.read_csv('test_ids.csv')['rid']
df = df[~df.rid.isin(valids)]
df_test = df[df.rid.isin(valids)]

skf_rids, skf_target = get_skf_target(df)

skf = StratifiedKFold(n_splits=5,shuffle=True,random_state=settings.SEED) #originally 5 splits
all_metrics = []


print(settings)
for fold,(idxT,idxV) in enumerate(skf.split(skf_rids, skf_target)):
    print('#'*25)
    print('### FOLD %i'%(fold+1))
    print('#'*25)
    
    K.clear_session()
    
    idxT, idxV = skf_rids[idxT], skf_rids[idxV]
    
    df_train = df[df.rid.isin(idxT)].reset_index(drop=True)
    df_val = df[df.rid.isin(idxV)].reset_index(drop=True)
    
    inpT, targetT = get_tokens_target(df_train)
    inpV, targetV = get_tokens_target(df_val)
    
    model = build_model(inpT)
        
    #sv = tf.keras.callbacks.ModelCheckpoint(
    #    '%s-roberta-%i.h5'%(VER,fold), monitor='val_loss', verbose=1, save_best_only=True,
    #    save_weights_only=True, mode='auto', save_freq='epoch')
    
    # sort the validation data #ПОКА НЕ СОРТИРУЕМ
#     shuffleV = np.int32(sorted(range(len(inpV[0])), key=lambda k: (inpV[0][k] == PAD_ID).sum(), reverse=True))
#     inpV = [arr[shuffleV] for arr in inpV]
#     targetV = targetV[shuffleV]
#     df_val = df_val.loc[shuffleV]
    
    for epoch in range(1, settings.EPOCHS+1):
        # sort and shuffle: We add random numbers to not have the same order in each epoch
        shuffleT = np.int32(sorted(range(len(inpT[0])), key=lambda k: (inpT[0][k] == settings.PAD_ID).sum() + np.random.randint(-3, 3), reverse=True))
        # shuffle in batches, otherwise short batches will always come in the beginning of each epoch
        num_batches = math.ceil(len(shuffleT) / settings.BATCH_SIZE)
        batch_inds = np.random.permutation(num_batches)
        shuffleT_ = []
        for batch_ind in batch_inds:
            shuffleT_.append(shuffleT[batch_ind * settings.BATCH_SIZE: (batch_ind + 1) * settings.BATCH_SIZE])
        shuffleT = np.concatenate(shuffleT_)
        # reorder the input data
        inpT = [arr[shuffleT] for arr in inpT]
        targetT = targetT[shuffleT]


        history = model.fit(inpT, targetT, 
            epochs=epoch, initial_epoch=epoch - 1, batch_size=settings.BATCH_SIZE, verbose=1, callbacks=[],
            validation_data=(inpV, targetV), shuffle=False)  # don't shuffle in `fit`
        

        preds = model.predict(inpV)
    
        losses = {
            'epoch':epoch,
            'loss': history.history['loss'][0], 
            'val_loss': history.history['val_loss'][0]
        }

        new_metrics = get_metrics(targetV, preds, df_val)
        if epoch == 1 :
            save_weights(model, f"{wandb.run.name}-{wandb.config.lang_model}-{fold}.h5")
            metrics = new_metrics
        else:
            if new_metrics['Main_AP'] > metrics['Main_AP']:
                save_weights(model, f"{wandb.run.name}-{wandb.config.lang_model}-{fold}.h5")
                metrics = new_metrics  
        wandb.log({**losses, **new_metrics})
        
    all_metrics.append(metrics)



# np.save(f"{wandb.run.name}-metrics.h5", all_metrics) 
wandb.log({
    "Mean_main_AP": np.mean([x['Main_AP'] for x in all_metrics]),
    "Mean_abs_sentence_rate": np.mean([x['abs_sentence_rate'] for x in all_metrics]),
    "Mean_weight_sentence_rate": np.mean([x['weight_sentence_rate'] for x in all_metrics]),

    
})

from eval import eval
all_metrics = eval(wandb.run.name, wandb.config.lang_model, fold)

wandb.log({
    "Mean_main_AP_test": np.mean([x['Main_AP'] for x in all_metrics]),
    "Mean_abs_sentence_rate_test": np.mean([x['abs_sentence_rate'] for x in all_metrics]),
    "Mean_weight_sentence_rate_test": np.mean([x['weight_sentence_rate'] for x in all_metrics]),

    
})
import pickle
from typing import List, Optional
from pathlib import Path
from typing import List, Tuple, Optional
from pydantic import BaseSettings
import tensorflow as tf
from transformers import RobertaConfig, TFRobertaModel
import sys
from typing import List
# import wandb 
from sklearn.metrics import average_precision_score, f1_score
import numpy as np
import tensorflow as tf
import tensorflow.keras.backend as K
from sklearn.model_selection import StratifiedKFold
from transformers import *
import tokenizers
import os
import re
from typing import Union
import json 
import pandas as pd
import math
import rules_spacy_model
from rules_spacy_model import nlp

# region Initialize prediction model: constants and tokenizer config

class Settings(BaseSettings):

    
    dir_lang_model: Path = Path(__file__).parent / 'roberta'
    dir_fit_model: Path = Path(__file__).parent / 'model'
    merge_file = dir_lang_model / 'roberta-base-merges.txt'
    vocab_file = dir_lang_model / 'roberta-base-vocab.json'
    lm_model_file = dir_lang_model / "roberta-base-tf_model.h5"
    lm_config_file = dir_lang_model / 'roberta-base-config.json'
    fit_model_file = dir_fit_model / "model_09_22.h5"


    config = RobertaConfig.from_pretrained(str(lm_config_file.resolve()))
    bert_model = TFRobertaModel.from_pretrained(str(lm_model_file.resolve()), config=config)


    tokenizer = tokenizers.ByteLevelBPETokenizer(
        vocab_file=str(vocab_file.resolve()), 
        merges_file=str(merge_file.resolve()), 
        lowercase=True,
        add_prefix_space=True
    )


    win_size: int = 3
    MAX_LEN = 220
    EPOCHS = 1
    BATCH_SIZE = 32
    LR = 3e-5
    PAD_ID = 1
    SEED = 1
    DROP_CLASSES = [
        'change/permission',
        'cancellation/fee/calculation',
        'cancellation/fee_per'
    ]
  
    cat_imp = {
        "cancellation/fare_combination": 1,
        "cancellation/fee/values": 5,          
        "cancellation/permission": 5,
        "cancellation/residual_amount": 1,
        "cancellation/taxes": 5,
        "no_show/calculation": 1,
        "no_show/definition": 1,
        "-": 1/30
    }
    cat2num = {
        "cancellation/fare_combination": 0,
        "cancellation/fee/values": 1,          
        "cancellation/permission": 2,
        "cancellation/residual_amount": 3,
        "cancellation/taxes": 4,
        "no_show/calculation": 5,
        "no_show/definition": 6,
        "-": 7
    }


    num2cat = dict(map(reversed, cat2num.items()))

    important_cls = [
        'cancellation/fee/values',
        'cancellation/permission',
        'cancellation/taxes']


settings = Settings()


if not (
        settings.merge_file.exists() and
        settings.vocab_file.exists() and
        settings.lm_model_file.exists() and
        settings.lm_config_file.exists() and
        settings.fit_model_file.exists()):
    raise FileNotFoundError("Model paths are not correct.")


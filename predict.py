from config import settings
from typing import List, Dict
from preprocessing import get_tokens
from model import model

def get_predict(sents) -> List[List[Dict]]:

    tokens = get_tokens(sents)
    preds = model.predict(tokens)

    ignored_indexes = [2, 8]
    predicts = []
    for arr in preds:
        if max(arr) > .8 and arr.argmax() in ignored_indexes:
            predicts.append([])
        elif max(arr) > .8 and arr.argmax() not in ignored_indexes:
            predicts.append(
                [
                    {
                        'category': settings.num2cat[arr.argmax()],
                        'weight': max(arr)
                    }
                ]
            )
        else:
            arr = pd.Series(arr)
            top = arr[~arr.index.isin(ignored_indexes)].sort_values(ascending=False)
            top = top[top > 1 / len(num2cat)][:3]
            classes = [num2cat[i] for i in top.index]
            vals = top.values
            pred = [{'category':cl, 'weight':v} for cl, v in zip(classes, vals)]
            predicts.append(pred if pred else None)
    return predicts

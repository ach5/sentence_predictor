import sys
from config import settings
# import wandb
from sklearn.metrics import average_precision_score, f1_score
import pandas as pd, numpy as np
import tensorflow as tf
import tensorflow.keras.backend as K
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.model_selection import StratifiedKFold
from transformers import *
import tokenizers
import os
import re
from typing import Union, List
import json 
import pandas as pd
import math
import rules_spacy_model
from rules_spacy_model import nlp

def get_metrics(target, preds, df, threshold=0.5):

        cat_w = np.array([settings.cat_imp[x] for x in settings.cat_imp])/sum(list(settings.cat_imp.values()))

        cat_w = dict(zip(settings.cat_imp, cat_w))
        
        
        metrics_common = {
            'AP_micro': average_precision_score(target, preds, average='micro'),
            'AP_macro': average_precision_score(target, preds, average='macro'),
            'AP_weight': average_precision_score(target, preds, average='weighted'),
            'f1_micro': f1_score(target, preds>threshold, average='micro'),
            'f1_macro': f1_score(target, preds>threshold, average='macro'),
            'f1_weighted': f1_score(target, preds>threshold, average='weighted')
        }
        metrics_class = dict(zip(
            [x.replace('/subrules[0]/', '') for x in settings.cat2num],
            [average_precision_score(target[:,cat], preds[:,cat]) for cat in settings.num2cat]
        ))
        
        
        ids = [df[df.rid==rid].index for rid in df.rid.unique()]

        weight_sentence_rate = []
        for ids_ in ids:
            # ids_ - список айдишников предложений одного правила
            t_ = target[ids_].argmax(axis=1) 
            p_ = pred[ids_].argmax(axis=1)
            
            # sent_weights вес каждого предложения в правиле
            sent_weights = [settings.cat_imp[settings.num2cat[numcat]] for numcat in t_]
            
            # equal_weights 
            equal_weights = (t_ == p_) * sent_weights
            # penalty 
            
            # penalty_coeff - насколько сильно штрафуем лишние предсказания
            penalty_coeff = .5
            penalty_weights = ~(t_ == p_) *  sent_weights * penalty_coeff
            sent_weight_rate = sum(equal_weights - penalty_weights)/sum(sent_weights)

            weight_sentence_rate.append(sent_weight_rate)
        
        weight_sentence_rate = np.mean(weight_sentence_rate)
        
        abs_sentence_rate = sum([(target[i] == (preds>threshold)[i]).all() for i in ids])/len(df.rid.unique())
        
        main_avp= {'Main_AP': sum([metrics_class[cat]* cat_w[cat] for cat in metrics_class]),
                'abs_sentence_rate': abs_sentence_rate
                'weight_sentence_rate':  weight_sentence_rate
                }
        return {**main_avp, **metrics_common, **metrics_class}


def get_skf_target(df):
    mlb = MultiLabelBinarizer()
    mlb.fit(df[['rid', 'y']].groupby('rid').sum().y)
    # OHE для наших классов
    mlb_df = pd.DataFrame(mlb.transform(df[['rid', 'y']].groupby('rid').sum().y), columns=mlb.classes_)

    # берем уникальные сочетания important_cls
    skf_map: List[List[int]] = np.unique(mlb_df[settings.important_cls].values, axis=0).tolist()

    # ids для кросс-вала
    skf_rids: List[str] = df[['rid', 'y']].groupby('rid').sum().y.index

    # переводим это в номера классов
    skf_target: List[int] = [skf_map.index(x.tolist()) for x in mlb_df[settings.important_cls].values]
    return skf_rids, np.array(skf_target)


def create_optimizer(
    init_lr,
    num_train_steps,
    num_warmup_steps,
    min_lr_ratio=0.0,
    adam_epsilon=1e-8,
    weight_decay_rate=0.0,
    include_in_weight_decay=None,
):
    """Creates an optimizer with learning rate schedule."""
    # Implements linear decay of the learning rate.
    lr_schedule = tf.keras.optimizers.schedules.PolynomialDecay(
        initial_learning_rate=init_lr,
        decay_steps=num_train_steps - num_warmup_steps,
        end_learning_rate=init_lr * min_lr_ratio,
    )
    if num_warmup_steps:
        lr_schedule = WarmUp(
            initial_learning_rate=init_lr, decay_schedule_fn=lr_schedule, warmup_steps=num_warmup_steps,
        )
    if weight_decay_rate > 0.0:
        optimizer = AdamWeightDecay(
            learning_rate=lr_schedule,
            weight_decay_rate=weight_decay_rate,
            beta_1=0.9,
            beta_2=0.999,
            epsilon=adam_epsilon,
            exclude_from_weight_decay=["LayerNorm", "layer_norm", "bias"],
            include_in_weight_decay=include_in_weight_decay,
        )
    else:
        optimizer = tf.keras.optimizers.Adam(learning_rate=lr_schedule, epsilon=adam_epsilon)
    # We return the optimizer and the LR scheduler in order to better track the
    # evolution of the LR independently of the optimizer.
    return optimizer

import pickle

def save_weights(model, dst_fn):
    weights = model.get_weights()
    with open(dst_fn, 'wb') as f:
        pickle.dump(weights, f)


def load_weights(model, weight_fn):
    with open(weight_fn, 'rb') as f:
        weights = pickle.load(f)
    model.set_weights(weights)
    return model


def build_model(inputs=None):

    len_inputs = len(inputs[0]) if inputs else 10
    ids = tf.keras.layers.Input((settings.MAX_LEN,), dtype=tf.int32)
    att = tf.keras.layers.Input((settings.MAX_LEN,), dtype=tf.int32)
    tok = tf.keras.layers.Input((settings.MAX_LEN,), dtype=tf.int32)
    x = settings.bert_model(ids, attention_mask=att, token_type_ids=tok)
    x1 = tf.keras.layers.Dropout(0.1)(x[0])
    x1 = tf.keras.layers.Conv1D(
        filters=20,
        kernel_size=3,
        padding="valid",
        activation="relu")(x1)
    x1 = tf.keras.layers.LeakyReLU()(x1)
    x1 = tf.keras.layers.Flatten()(x1)
    x1 = tf.keras.layers.Dense(len(settings.cat2num))(x1)
    x1 = tf.keras.layers.Activation('softmax')(x1)
    model = tf.keras.models.Model(inputs=[ids, att, tok], outputs=x1)
    train_steps = int(len_inputs * 0.8 * settings.EPOCHS/settings.BATCH_SIZE)
    warmup_steps = int(0.1 * train_steps)
    optimizer = create_optimizer(settings.LR, train_steps, warmup_steps)
    model.compile(loss='binary_crossentropy', optimizer=optimizer)
    return model

# model = build_model()
# model = load_weights(model, str(settings.fit_model_file.resolve()))
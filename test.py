from predict import get_predict

sents = """"UNLESS OTHERWISE SPECIFIED   NOTE - RULE CT16 IN IPRG\nAPPLIES\nUNLESS OTHERWISE SPECIFIED\n  CANCELLATIONS\n    ANY TIME\n      TICKET IS NON-REFUNDABLE."""
preds = get_predict(sents.split('\n'))

print(preds)